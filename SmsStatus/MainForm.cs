﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SmsStatus
{
    public partial class MainForm : Form
    {
        ProcessStartInfo start;
        Process proc;
        bool uiMainLaunched = false;

        List<SMSStatus> statuses;
        List<SMSStatus> statusCache;

        BindingSource statusesSource;
        BindingList<SMSStatus> bindingList;

        Dictionary<DateTime, int> dictDayLastNo; //number of sms for valid enumeration
        Dictionary<string, string> dictSources;

        List<string> listSources;

        private int lastId = 0;

        private DateTime currentDate;
        private SMSStatus lastStatus;

        private SmsToTextFileSaver smsSaver;
        private SmsFromTextFileLoader smsLoader;

        public class IniSettings
        {
            public SMSStatus LastStatus;
            public int LastId;
        }

        public MainForm()
        {
            InitializeComponent();

            dictSources = new Dictionary<string, string>();
            listSources = new List<string>();
            statusCache = new List<SMSStatus>();
            dictDayLastNo = new Dictionary<DateTime, int>();

            smsSaver = new SmsToTextFileSaver();
            smsLoader = new SmsFromTextFileLoader(GetCurrentFileName());

            LoadSourcesDict();

        }


        private void OnAddingNewToBindingSource(object sender, AddingNewEventArgs e)
        {
            if (sms_dataGridView.Rows.Count == bindingList.Count)
            {
                bindingList.RemoveAt(bindingList.Count - 1);
            }
        }


        private void LoadSourcesDict()
        {
            const int SENDER = 0;
            const int CAPTION = 1;

            string[] lines = File.ReadAllLines(@"senders.txt");
            foreach (var line in lines)
            {
                var items = line.Split(';');
                dictSources.Add(items[SENDER], items[CAPTION]);
                listSources.Add(items[CAPTION]);
                sender_comboBox.Items.Add(String.Format("{0} - {1}", items[CAPTION], items[SENDER]));
            }
        }


        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadSettings();
                LoadIniDb();

                LaunchUIMain();

                statuses = new List<SMSStatus>();

                bindingList = new BindingList<SMSStatus>(statuses);
                bindingList.AddingNew += OnAddingNewToBindingSource;

                statusesSource = new BindingSource(bindingList, null);
                sms_dataGridView.DataSource = statusesSource;

                CurrentDate = DateTime.Now.Date;

                receiveSMS();

                refreshTimer.Enabled = true;
            }
            catch (Exception ex)
            {
                Log("Error:" + ex.Message);
            }
        }

        const string c_iniDb = "ini.db";
        private void LoadIniDb()
        {
            if(!File.Exists(c_iniDb))
            {
                CreateIniDb();
            }
            LoadSettingsDb();
        }

        private void LoadSettingsDb() 
        {
            using (var conn = GetIniDbConnection())
            {
                string sql = @"SELECT last_no, date FROM status_sms;";
                dictDayLastNo.Clear();
                try
                {
                    using (var fmd = GenerateCommand(conn, sql))
                    {
                        using (SQLiteDataReader r = fmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                dictDayLastNo.Add(DateTime.Parse(r.GetString(1)), r.GetInt32(0));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log("Error:" + ex.Message);
                }
            }
        }

        private SQLiteConnection GetIniDbConnection()
        {
            var conn = new SQLiteConnection(String.Format("Data Source={0}; Version=3;", c_iniDb));
            try
            {
                conn.Open();
                return conn;
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        private void CreateIniDb()
        {
            var conn = GetIniDbConnection(); 

            if (conn.State == ConnectionState.Open)
            {
                CreateSmsStatusTable(conn);
            }

            conn.Dispose();
        }


        private void CreateSmsStatusTable(SQLiteConnection conn)
        {
            var sql_command = GenerateCommand(conn,
                "DROP TABLE IF EXISTS status_sms;"
              + "CREATE TABLE status_sms("
              + "id INTEGER PRIMARY KEY AUTOINCREMENT, "
              + "date TEXT, "
              + "last_no INTEGER "
              + ");");

            ExecuteCommandAndLogStatus(sql_command);

        }


        private void InsertSmsLastNoToDb(SQLiteConnection conn, DateTime date, int lastNo)
        {
            var sql_command = GenerateCommand(conn,
              "INSERT INTO status_sms (date, last_no) "
              + "VALUES ( "
              + "'" + date.ToShortDateString() + "',"
              + lastNo.ToString()
              + ");"
            );

            ExecuteCommandAndLogStatus(sql_command);
        }

        const int UNKNOWN_LAST_NO = -1;

        private int GetLastNoFromDb(SQLiteConnection conn, DateTime date)
        {
            string sql = @"SELECT MAX(last_no) as max_last_no FROM status_sms WHERE date = "
              + "'" + date.ToShortDateString() + "';";

            try
            {
                using (var fmd = GenerateCommand(conn, sql))
                {
                    using (SQLiteDataReader r = fmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            return r.GetInt32(0);
                        }
                    }
                }
            }catch( Exception )
            {
                return UNKNOWN_LAST_NO;
            }

            return UNKNOWN_LAST_NO;
        }


        private void UpdateSmsLastNoInDb(SQLiteConnection conn, DateTime date, int lastNo)
        {
            var sql_command = GenerateCommand(conn,
              "UPDATE status_sms "
              + "SET last_no = "
              + lastNo.ToString()
              + " WHERE date = "
              + "'" + date.ToShortDateString() + "';"
            );

            ExecuteCommandAndLogStatus(sql_command);
        }


        private SQLiteCommand GenerateCommand( SQLiteConnection conn, string text )
        {
            SQLiteCommand cmd = conn.CreateCommand();
            cmd.CommandText = text;
            cmd.CommandType = CommandType.Text;
            return cmd;
        }


        private void ExecuteCommandAndLogStatus(SQLiteCommand cmd)
        {
            try
            {
                cmd.ExecuteNonQuery();
                #if DEBUG
                    Log(String.Format("Command {0} executed succesfully", cmd.CommandText ));
                #endif
            }
            catch (SQLiteException ex)
            {
                Log("Error:: " + ex.Message);
            }
        }


        private void LoadSettings()
        {
            using (Stream stream = new FileStream("sms_status_settings.xml", FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(IniSettings));

                var iniSet = (IniSettings)serializer.Deserialize(stream);

                lastId = iniSet.LastId;
                lastStatus = iniSet.LastStatus;
            }
        }


        private void Log(string msg)
        {
            log_textBox.AppendText(msg + "\n");
        }


        private void LoadSMSFromFile()
        {
            var fileName = GetCurrentFileName();

            bindingList.Clear();
            statusCache.Clear();

            labelLoading.Visible = true;
            labelNoSMSOnDate.Visible = false;

            if (!File.Exists(fileName))
            {
                labelLoading.Visible = false;
                labelNoSMSOnDate.Visible = true;
                return;
            }

            string[] lines = File.ReadAllLines(fileName);
            try
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    string[] idPart = lines[i].Split(';');
                    int ID = Convert.ToInt32(idPart[1]);
                    var caption = idPart[2];
                    var content = idPart[3];
                    var timeStr = idPart[4];

                    var status = new SMSStatus();
                    status.No = i + 1;
                    status.Id = ID;
                    status.Sender = caption;
                    status.Content = content;
                    status.Time = timeStr;

                    lastStatus = status;

                    bindingList.Add(status);
                    statusCache.Add(status);
                }
            }catch(Exception e)
            {
                Log("Error:" + e.Message);
            }

            labelLoading.Visible = false;
            labelNoSMSOnDate.Visible = lines.Length == 0;
        }


        private void LaunchUIMain()
        {
            if (IsProcessOpen("UIMain"))
                return;

            start = new ProcessStartInfo();
            start.FileName = "UIMain.exe";
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.CreateNoWindow = true;

            proc = Process.Start(start);

            uiMainLaunched = true;
        }


        public bool IsProcessOpen(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName.Contains(name))
                {
                    return true;
                }
            }

            return false;
        }


        private void receiveButton_Click(object sender, EventArgs e)
        {
            receiveSMS();
        }


        public DateTime CurrentDate {
            get { return currentDate; }
            private set
            {
                currentDate = value;
                SetCurrentDateUI();
            }
        }

        public DateTime TodayDate { get { return DateTime.Now.Date;  } }

        private void SetCurrentDateUI()
        {
            current_dateTimePicker.Value = currentDate;
        }


        private void receiveSMS()
        {
            this.smsTableAdapter.Fill(this.dbDataSet.SMS);
            var row = (dbDataSet.SMSRow)dbDataSet.SMS.Rows[dbDataSet.SMS.Count - 1];

            if(row.ID == lastId)
                return;

            int endId = row.ID;
            int endPos = dbDataSet.SMS.Count;
            int startPos = endPos - ( endId - lastId );

            for (int i = startPos; i < endPos; i++)
            {
                row = (dbDataSet.SMSRow)dbDataSet.SMS.Rows[i];

                SaveSmsFromRow(row);
            }

            if (CurrentDate == TodayDate )
            {
                ScrollToLastSms();
            }

        }


        private void SaveSmsFromRow(dbDataSet.SMSRow row)
        {
            if (!dictSources.ContainsKey(row.Sender))
            {
                Log("New sender found: " + row.Sender);
                return;
            }

            lastId = row.ID;

            string sendCapt = GetSourceCaption(row.Sender);

            int lastNo = IncLastNoForToday();

            dictDayLastNo[TodayDate] = lastNo;

            InsertOrUpdateTodayLastNoInDb(lastNo);

            int rid = row.ID;
            var content = row.Content;

            SMSStatus status = MakeSmsStatus(sendCapt, lastNo, rid, content);

            labelNoSMSOnDate.Visible = false;

            if (CurrentDate == TodayDate)//visualise sms
            {
                bindingList.Add(status);
            }
            smsSaver.SaveToDate(TodayDate, status);
        }


        private static SMSStatus MakeSmsStatus(string sendCapt, int lastNo, int rid, string content)
        {
            var status = new SMSStatus();
            status.No = lastNo;
            status.Id = rid;
            status.Sender = sendCapt;
            status.Content = content;
            status.Time = DateTime.Now.ToShortTimeString();
            return status;
        }


        private void InsertOrUpdateTodayLastNoInDb(int lastNo)
        {
            using (var conn = GetIniDbConnection())
            {
                if (GetLastNoFromDb(conn, TodayDate) == UNKNOWN_LAST_NO)
                    InsertSmsLastNoToDb(conn, TodayDate, lastNo);
                else
                    UpdateSmsLastNoInDb(conn, TodayDate, lastNo);
            }
        }


        private int IncLastNoForToday()
        {
            int lastNo;
            var date = TodayDate;
            if (!dictDayLastNo.TryGetValue(date, out lastNo))
            {
                dictDayLastNo.Add(date, 0);
                lastNo = GetLastNoFromIniDb(TodayDate);
            }

            if (CurrentDate == TodayDate)//we have visual list of sms-es
            {
                if (lastNo <= bindingList.Count)
                    lastNo = bindingList.Count;
            }
            else
                lastNo = GetLastNoFromIniDb(TodayDate);

            lastNo += 1;
            dictDayLastNo[date] = lastNo;
            return lastNo;
        }


        private int GetLastNoFromIniDb(DateTime date)
        {
            using (var conn = GetIniDbConnection())
            {
                return GetLastNoFromDb(conn, date);
            }
        }


        private string GetSourceCaption(string sender)
        {
            return dictSources[sender];
        }


        private void ScrollToLastSms()
        {
            var displayed = sms_dataGridView.DisplayedRowCount(true);
            var row = displayed - 1;
            sms_dataGridView.FirstDisplayedScrollingRowIndex = row; 
        }


        private string GetCurrentFileName()
        {
            return GetFileName(currentDate);
        }


        private string GetFileName( DateTime value)
        {
            var day = value.Day;
            var month = value.Month;
            var year = value.Year;

            return String.Format("sms{0}-{1}-{2}.txt", day, month, year);
        }


        private void buttonDelete_Click(object sender, EventArgs e)
        {
            this.sms_dataGridView.Rows.RemoveAt(0);
            this.dbDataSet.AcceptChanges();
        }


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (uiMainLaunched)
            {
                //TODO: Close by sending windows messages
                //proc.Kill();
                uiMainLaunched = false;
            }

            SaveSettings();
        }


        private void SaveSettings()
        {
            var settings = new IniSettings();
            settings.LastId = lastId;
            settings.LastStatus = lastStatus;

            using (Stream writer = new FileStream("sms_status_settings.xml", FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(IniSettings));
                serializer.Serialize(writer, settings);
            }
        }


        int tickCount = 0; //for refresh timer
        const int MAX_TICKS = 5;
        bool querying = false;

        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            if( querying)
            {
                return;
            }

            tickCount += 1;
            if( tickCount < MAX_TICKS)
            {
                labelQueryTimerStatus.Text = labelQueryTimerStatus.Text + '.';
                return;
            }

            labelQueryTimerStatus.Text = "!";
            tickCount = 0;
            querying = true;

            this.Invoke(new MethodInvoker(delegate
            {
                receiveSMS();
                labelQueryTimerStatus.Text = "";
                querying = false;
            }));
        }


        private String currentSender; //for filtering

        private void FilterSMSBy(string sender)
        {
            currentSender = sender;
            statuses = statusCache.FindAll(EqualToCurrentSender);

            ReloadBindingListWithUI();
        }

        private void ReloadBindingListWithUI()
        {
            CopyStatusesToBindingList();
            UpdateBindingListUI();
        }

        private void UpdateBindingListUI()
        {
            bindingList.ResetBindings();
            sms_dataGridView.Refresh();
        }

        private void CopyStatusesToBindingList()
        {
            bindingList.Clear();
            for (int i = 0; i < statuses.Count; i++)
            {
                bindingList.Add(statuses[i]);
            }
        }

        private bool EqualToCurrentSender(SMSStatus obj)
        {
            return obj.Sender.Equals(currentSender);
        }

        private void sms_dataGridView_Click(object sender, EventArgs e)
        {
            MarkCurrentItemAsReaden();
        }

        private void MarkCurrentItemAsReaden()
        {
            SMSStatus item = GetCurrentSelectedItem();
            item.Readen = true;
            HighlightCurrentRow();
        }

        private void HighlightCurrentRow()
        {
            GetCurrentRow().DefaultCellStyle.BackColor = Color.AliceBlue;
        }

        private SMSStatus GetCurrentSelectedItem()
        {
            return bindingList[GetCurrentRow().Index];
        }

        private DataGridViewRow GetCurrentRow()
        {
            if (sms_dataGridView.RowCount == 0)
                return null;

            return sms_dataGridView.SelectedRows[0];
        }

        private void sms_dataGridView_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if( bindingList[e.RowIndex].Readen)
            {
                sms_dataGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Beige;
            }
            else
            {
                var curColor = Color.White;
                if (currentDate.Date != DateTime.Now.Date)
                    curColor = Color.LightGray;

                sms_dataGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = curColor;
            }
        }

        private void current_dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            LoadSMSToCurrentDate();
        }

        private void  LoadSMSToCurrentDate()
        {
            currentDate = current_dateTimePicker.Value;
            if( currentDate.Date == DateTime.Now.Date )
            {
                sms_dataGridView.BackgroundColor = Color.White;
            }
            else
            {
                sms_dataGridView.BackgroundColor = Color.LightGray;
            }

            current_dateTimePicker.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                ClearSMSFilter();
                LoadSMSFromFile();
            }
            finally
            {
                current_dateTimePicker.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            FilterSMSBy(listSources[sender_comboBox.SelectedIndex]);
        }

        private void buttonShowAllSMS_Click(object sender, EventArgs e)
        {
            ClearSMSFilter();
        }

        private void ClearSMSFilter()
        {
            statuses = new List<SMSStatus>(statusCache);

            sender_comboBox.SelectedIndex = -1;

            ReloadBindingListWithUI();
        }
    }
}
