﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SmsStatus
{
    public class SMSStatus : INotifyPropertyChanged
    {
        private int no;
        private string sender;
        private string content;
        private int id;
        private String time;
        private Boolean readen;

        public int Id
        {
            get { return id; }
            set
            {
                id = value;
                NotifyPropertyChanged();
            }
        }

        public int No {
            get { return no; }
            set
            {
                no = value;
                NotifyPropertyChanged();
            }
        }

        public string Sender {
            get { return sender; }
            set
            {
                sender = value;
                NotifyPropertyChanged();
            }
        }

        public string Content {
            get { return content; }
            set
            {
                content = value;
                NotifyPropertyChanged();
            }
        }

        public String Time {
            get { return time; }
            set {
                time = value;
                NotifyPropertyChanged();
            }
        }


        public Boolean Readen {
            get { return readen; }
            set
            {
                readen = value;
                NotifyPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged( String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
