﻿namespace SmsStatus
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.smsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dbDataSet = new SmsStatus.dbDataSet();
            this.sms_dataGridView = new System.Windows.Forms.DataGridView();
            this.noDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.senderDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.readenDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sMSStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.receiveButton = new System.Windows.Forms.Button();
            this.smsTableAdapter = new SmsStatus.dbDataSetTableAdapters.SMSTableAdapter();
            this.tableAdapterManager = new SmsStatus.dbDataSetTableAdapters.TableAdapterManager();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.refreshTimer = new System.Windows.Forms.Timer(this.components);
            this.log_textBox = new System.Windows.Forms.TextBox();
            this.sender_comboBox = new System.Windows.Forms.ComboBox();
            this.filterButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.smsindexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.senderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.receiverDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendernameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recvTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sMSCentreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.locationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reservedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isConcatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.concatRefIndexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.concatTotalMountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.concatSeqDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.current_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.labelNoSMSOnDate = new System.Windows.Forms.Label();
            this.labelLoading = new System.Windows.Forms.Label();
            this.buttonShowAllSMS = new System.Windows.Forms.Button();
            this.labelQueryTimerStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.smsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sms_dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sMSStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // smsBindingSource
            // 
            this.smsBindingSource.DataMember = "SMS";
            this.smsBindingSource.DataSource = this.dbDataSet;
            // 
            // dbDataSet
            // 
            this.dbDataSet.DataSetName = "dbDataSet";
            this.dbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sms_dataGridView
            // 
            this.sms_dataGridView.AllowUserToAddRows = false;
            this.sms_dataGridView.AllowUserToDeleteRows = false;
            this.sms_dataGridView.AllowUserToResizeRows = false;
            this.sms_dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sms_dataGridView.AutoGenerateColumns = false;
            this.sms_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sms_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.noDataGridViewTextBoxColumn,
            this.senderDataGridViewTextBoxColumn1,
            this.contentDataGridViewTextBoxColumn1,
            this.timeDataGridViewTextBoxColumn,
            this.readenDataGridViewCheckBoxColumn});
            this.sms_dataGridView.DataSource = this.sMSStatusBindingSource;
            this.sms_dataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.sms_dataGridView.Location = new System.Drawing.Point(12, 79);
            this.sms_dataGridView.MultiSelect = false;
            this.sms_dataGridView.Name = "sms_dataGridView";
            this.sms_dataGridView.ReadOnly = true;
            this.sms_dataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sms_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sms_dataGridView.Size = new System.Drawing.Size(720, 439);
            this.sms_dataGridView.TabIndex = 1;
            this.sms_dataGridView.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.sms_dataGridView_RowPrePaint);
            this.sms_dataGridView.Click += new System.EventHandler(this.sms_dataGridView_Click);
            // 
            // noDataGridViewTextBoxColumn
            // 
            this.noDataGridViewTextBoxColumn.DataPropertyName = "No";
            this.noDataGridViewTextBoxColumn.HeaderText = "No";
            this.noDataGridViewTextBoxColumn.Name = "noDataGridViewTextBoxColumn";
            this.noDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // senderDataGridViewTextBoxColumn1
            // 
            this.senderDataGridViewTextBoxColumn1.DataPropertyName = "Sender";
            this.senderDataGridViewTextBoxColumn1.HeaderText = "Блок мониторинга";
            this.senderDataGridViewTextBoxColumn1.Name = "senderDataGridViewTextBoxColumn1";
            this.senderDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // contentDataGridViewTextBoxColumn1
            // 
            this.contentDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.contentDataGridViewTextBoxColumn1.DataPropertyName = "Content";
            this.contentDataGridViewTextBoxColumn1.HeaderText = "Содержимое";
            this.contentDataGridViewTextBoxColumn1.Name = "contentDataGridViewTextBoxColumn1";
            this.contentDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // timeDataGridViewTextBoxColumn
            // 
            this.timeDataGridViewTextBoxColumn.DataPropertyName = "Time";
            this.timeDataGridViewTextBoxColumn.HeaderText = "Время";
            this.timeDataGridViewTextBoxColumn.Name = "timeDataGridViewTextBoxColumn";
            this.timeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // readenDataGridViewCheckBoxColumn
            // 
            this.readenDataGridViewCheckBoxColumn.DataPropertyName = "Readen";
            this.readenDataGridViewCheckBoxColumn.HeaderText = "Прочтено";
            this.readenDataGridViewCheckBoxColumn.Name = "readenDataGridViewCheckBoxColumn";
            this.readenDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // sMSStatusBindingSource
            // 
            this.sMSStatusBindingSource.DataSource = typeof(SmsStatus.SMSStatus);
            // 
            // receiveButton
            // 
            this.receiveButton.Location = new System.Drawing.Point(12, 12);
            this.receiveButton.Name = "receiveButton";
            this.receiveButton.Size = new System.Drawing.Size(75, 23);
            this.receiveButton.TabIndex = 2;
            this.receiveButton.Text = "Receive";
            this.receiveButton.UseVisualStyleBackColor = true;
            this.receiveButton.Visible = false;
            this.receiveButton.Click += new System.EventHandler(this.receiveButton_Click);
            // 
            // smsTableAdapter
            // 
            this.smsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.SMSTableAdapter = this.smsTableAdapter;
            this.tableAdapterManager.UpdateOrder = SmsStatus.dbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(93, 12);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 3;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Visible = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // refreshTimer
            // 
            this.refreshTimer.Enabled = true;
            this.refreshTimer.Interval = 1000;
            this.refreshTimer.Tick += new System.EventHandler(this.refreshTimer_Tick);
            // 
            // log_textBox
            // 
            this.log_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.log_textBox.Location = new System.Drawing.Point(12, 524);
            this.log_textBox.Multiline = true;
            this.log_textBox.Name = "log_textBox";
            this.log_textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.log_textBox.Size = new System.Drawing.Size(719, 129);
            this.log_textBox.TabIndex = 4;
            // 
            // sender_comboBox
            // 
            this.sender_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sender_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sender_comboBox.FormattingEnabled = true;
            this.sender_comboBox.Location = new System.Drawing.Point(392, 50);
            this.sender_comboBox.Name = "sender_comboBox";
            this.sender_comboBox.Size = new System.Drawing.Size(146, 21);
            this.sender_comboBox.TabIndex = 5;
            // 
            // filterButton
            // 
            this.filterButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.filterButton.Location = new System.Drawing.Point(544, 48);
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size(91, 23);
            this.filterButton.TabIndex = 6;
            this.filterButton.Text = "Фильтровать";
            this.filterButton.UseVisualStyleBackColor = true;
            this.filterButton.Click += new System.EventHandler(this.filterButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.smsindexDataGridViewTextBoxColumn,
            this.senderDataGridViewTextBoxColumn,
            this.receiverDataGridViewTextBoxColumn,
            this.sendernameDataGridViewTextBoxColumn,
            this.sendTimeDataGridViewTextBoxColumn,
            this.recvTimeDataGridViewTextBoxColumn,
            this.codeTypeDataGridViewTextBoxColumn,
            this.contentDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.sMSCentreDataGridViewTextBoxColumn,
            this.locationDataGridViewTextBoxColumn,
            this.reservedDataGridViewTextBoxColumn,
            this.isConcatDataGridViewTextBoxColumn,
            this.concatRefIndexDataGridViewTextBoxColumn,
            this.concatTotalMountDataGridViewTextBoxColumn,
            this.concatSeqDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.smsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 368);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(697, 150);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.Visible = false;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            // 
            // smsindexDataGridViewTextBoxColumn
            // 
            this.smsindexDataGridViewTextBoxColumn.DataPropertyName = "Smsindex";
            this.smsindexDataGridViewTextBoxColumn.HeaderText = "Smsindex";
            this.smsindexDataGridViewTextBoxColumn.Name = "smsindexDataGridViewTextBoxColumn";
            // 
            // senderDataGridViewTextBoxColumn
            // 
            this.senderDataGridViewTextBoxColumn.DataPropertyName = "Sender";
            this.senderDataGridViewTextBoxColumn.HeaderText = "Sender";
            this.senderDataGridViewTextBoxColumn.Name = "senderDataGridViewTextBoxColumn";
            // 
            // receiverDataGridViewTextBoxColumn
            // 
            this.receiverDataGridViewTextBoxColumn.DataPropertyName = "Receiver";
            this.receiverDataGridViewTextBoxColumn.HeaderText = "Receiver";
            this.receiverDataGridViewTextBoxColumn.Name = "receiverDataGridViewTextBoxColumn";
            // 
            // sendernameDataGridViewTextBoxColumn
            // 
            this.sendernameDataGridViewTextBoxColumn.DataPropertyName = "sendername";
            this.sendernameDataGridViewTextBoxColumn.HeaderText = "sendername";
            this.sendernameDataGridViewTextBoxColumn.Name = "sendernameDataGridViewTextBoxColumn";
            // 
            // sendTimeDataGridViewTextBoxColumn
            // 
            this.sendTimeDataGridViewTextBoxColumn.DataPropertyName = "SendTime";
            this.sendTimeDataGridViewTextBoxColumn.HeaderText = "SendTime";
            this.sendTimeDataGridViewTextBoxColumn.Name = "sendTimeDataGridViewTextBoxColumn";
            // 
            // recvTimeDataGridViewTextBoxColumn
            // 
            this.recvTimeDataGridViewTextBoxColumn.DataPropertyName = "RecvTime";
            this.recvTimeDataGridViewTextBoxColumn.HeaderText = "RecvTime";
            this.recvTimeDataGridViewTextBoxColumn.Name = "recvTimeDataGridViewTextBoxColumn";
            // 
            // codeTypeDataGridViewTextBoxColumn
            // 
            this.codeTypeDataGridViewTextBoxColumn.DataPropertyName = "CodeType";
            this.codeTypeDataGridViewTextBoxColumn.HeaderText = "CodeType";
            this.codeTypeDataGridViewTextBoxColumn.Name = "codeTypeDataGridViewTextBoxColumn";
            // 
            // contentDataGridViewTextBoxColumn
            // 
            this.contentDataGridViewTextBoxColumn.DataPropertyName = "Content";
            this.contentDataGridViewTextBoxColumn.HeaderText = "Content";
            this.contentDataGridViewTextBoxColumn.Name = "contentDataGridViewTextBoxColumn";
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            // 
            // sMSCentreDataGridViewTextBoxColumn
            // 
            this.sMSCentreDataGridViewTextBoxColumn.DataPropertyName = "SMSCentre";
            this.sMSCentreDataGridViewTextBoxColumn.HeaderText = "SMSCentre";
            this.sMSCentreDataGridViewTextBoxColumn.Name = "sMSCentreDataGridViewTextBoxColumn";
            // 
            // locationDataGridViewTextBoxColumn
            // 
            this.locationDataGridViewTextBoxColumn.DataPropertyName = "Location";
            this.locationDataGridViewTextBoxColumn.HeaderText = "Location";
            this.locationDataGridViewTextBoxColumn.Name = "locationDataGridViewTextBoxColumn";
            // 
            // reservedDataGridViewTextBoxColumn
            // 
            this.reservedDataGridViewTextBoxColumn.DataPropertyName = "Reserved";
            this.reservedDataGridViewTextBoxColumn.HeaderText = "Reserved";
            this.reservedDataGridViewTextBoxColumn.Name = "reservedDataGridViewTextBoxColumn";
            // 
            // isConcatDataGridViewTextBoxColumn
            // 
            this.isConcatDataGridViewTextBoxColumn.DataPropertyName = "IsConcat";
            this.isConcatDataGridViewTextBoxColumn.HeaderText = "IsConcat";
            this.isConcatDataGridViewTextBoxColumn.Name = "isConcatDataGridViewTextBoxColumn";
            // 
            // concatRefIndexDataGridViewTextBoxColumn
            // 
            this.concatRefIndexDataGridViewTextBoxColumn.DataPropertyName = "ConcatRefIndex";
            this.concatRefIndexDataGridViewTextBoxColumn.HeaderText = "ConcatRefIndex";
            this.concatRefIndexDataGridViewTextBoxColumn.Name = "concatRefIndexDataGridViewTextBoxColumn";
            // 
            // concatTotalMountDataGridViewTextBoxColumn
            // 
            this.concatTotalMountDataGridViewTextBoxColumn.DataPropertyName = "ConcatTotalMount";
            this.concatTotalMountDataGridViewTextBoxColumn.HeaderText = "ConcatTotalMount";
            this.concatTotalMountDataGridViewTextBoxColumn.Name = "concatTotalMountDataGridViewTextBoxColumn";
            // 
            // concatSeqDataGridViewTextBoxColumn
            // 
            this.concatSeqDataGridViewTextBoxColumn.DataPropertyName = "ConcatSeq";
            this.concatSeqDataGridViewTextBoxColumn.HeaderText = "ConcatSeq";
            this.concatSeqDataGridViewTextBoxColumn.Name = "concatSeqDataGridViewTextBoxColumn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(13, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Рабочий журнал за";
            // 
            // current_dateTimePicker
            // 
            this.current_dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.current_dateTimePicker.Location = new System.Drawing.Point(189, 53);
            this.current_dateTimePicker.Name = "current_dateTimePicker";
            this.current_dateTimePicker.Size = new System.Drawing.Size(132, 20);
            this.current_dateTimePicker.TabIndex = 9;
            this.current_dateTimePicker.ValueChanged += new System.EventHandler(this.current_dateTimePicker_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(597, 25);
            this.label2.TabIndex = 10;
            this.label2.Text = "GSM система дистаницонного мониторинга объектов";
            // 
            // labelNoSMSOnDate
            // 
            this.labelNoSMSOnDate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNoSMSOnDate.AutoSize = true;
            this.labelNoSMSOnDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNoSMSOnDate.Location = new System.Drawing.Point(22, 126);
            this.labelNoSMSOnDate.Name = "labelNoSMSOnDate";
            this.labelNoSMSOnDate.Size = new System.Drawing.Size(161, 20);
            this.labelNoSMSOnDate.TabIndex = 11;
            this.labelNoSMSOnDate.Text = "Нет данных на дату";
            this.labelNoSMSOnDate.Visible = false;
            // 
            // labelLoading
            // 
            this.labelLoading.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLoading.AutoSize = true;
            this.labelLoading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelLoading.Location = new System.Drawing.Point(22, 156);
            this.labelLoading.Name = "labelLoading";
            this.labelLoading.Size = new System.Drawing.Size(191, 20);
            this.labelLoading.TabIndex = 12;
            this.labelLoading.Text = "Загружаю данные дня...";
            this.labelLoading.Visible = false;
            // 
            // buttonShowAllSMS
            // 
            this.buttonShowAllSMS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShowAllSMS.Location = new System.Drawing.Point(641, 48);
            this.buttonShowAllSMS.Name = "buttonShowAllSMS";
            this.buttonShowAllSMS.Size = new System.Drawing.Size(91, 23);
            this.buttonShowAllSMS.TabIndex = 13;
            this.buttonShowAllSMS.Text = "Показать всё";
            this.buttonShowAllSMS.UseVisualStyleBackColor = true;
            this.buttonShowAllSMS.Click += new System.EventHandler(this.buttonShowAllSMS_Click);
            // 
            // labelQueryTimerStatus
            // 
            this.labelQueryTimerStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelQueryTimerStatus.AutoSize = true;
            this.labelQueryTimerStatus.Location = new System.Drawing.Point(638, 22);
            this.labelQueryTimerStatus.Name = "labelQueryTimerStatus";
            this.labelQueryTimerStatus.Size = new System.Drawing.Size(0, 13);
            this.labelQueryTimerStatus.TabIndex = 14;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 665);
            this.Controls.Add(this.labelQueryTimerStatus);
            this.Controls.Add(this.buttonShowAllSMS);
            this.Controls.Add(this.labelLoading);
            this.Controls.Add(this.labelNoSMSOnDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.current_dateTimePicker);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.filterButton);
            this.Controls.Add(this.sender_comboBox);
            this.Controls.Add(this.log_textBox);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.receiveButton);
            this.Controls.Add(this.sms_dataGridView);
            this.Name = "MainForm";
            this.Text = "SMS Статус системы";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.smsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sms_dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sMSStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private dbDataSet dbDataSet;
        private System.Windows.Forms.BindingSource smsBindingSource;
        private dbDataSetTableAdapters.SMSTableAdapter smsTableAdapter;
        private dbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView sms_dataGridView;
        private System.Windows.Forms.Button receiveButton;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Timer refreshTimer;
        private System.Windows.Forms.TextBox log_textBox;
        private System.Windows.Forms.ComboBox sender_comboBox;
        private System.Windows.Forms.Button filterButton;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn smsindexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn senderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn receiverDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendernameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn recvTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sMSCentreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn locationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reservedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isConcatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn concatRefIndexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn concatTotalMountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn concatSeqDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker current_dateTimePicker;
        private System.Windows.Forms.BindingSource sMSStatusBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn noDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn senderDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn readenDataGridViewCheckBoxColumn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelNoSMSOnDate;
        private System.Windows.Forms.Label labelLoading;
        private System.Windows.Forms.Button buttonShowAllSMS;
        private System.Windows.Forms.Label labelQueryTimerStatus;
    }
}

