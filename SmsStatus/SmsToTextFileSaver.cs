﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SmsStatus
{
    class SmsToTextFileSaver
    {
        internal void SaveToDate(DateTime date, SMSStatus status)
        {
            var fileName = GetFileName(date);

            using (var w = File.AppendText(fileName))
            {
                w.WriteLine(String.Format("{0};{3};{1};{2};{4}",status.No, status.Sender, status.Content, status.Id, status.Time));
            }
        }

        private string GetFileName( DateTime value)
        {
            var day = value.Day;
            var month = value.Month;
            var year = value.Year;

            return String.Format("sms{0}-{1}-{2}.txt", day, month, year);
        }

    }
}
