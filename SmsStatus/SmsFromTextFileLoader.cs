﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SmsStatus
{
    class SmsFromTextFileLoader : IEnumerable<SMSStatus>
    {
        private string fileName = "";

        public SmsFromTextFileLoader( string afileName )
        {
            fileName = afileName;
        }

        public IEnumerator<SMSStatus> GetEnumerator()
        {
            return new FileReaderEnumerator(fileName);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }

    class FileReaderEnumerator : IEnumerator<SMSStatus>
    {
        private string fileName;

        public FileReaderEnumerator(string fileName)
        {
            this.fileName = fileName;
        }

        public SMSStatus Current
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        object IEnumerator.Current
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool MoveNext()
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
